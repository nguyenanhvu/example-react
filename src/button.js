import React from "react";

export default function Button(props) {
  const { kind, ...other } = props;
  return (
    <button kind="primary" {...other}>
      {other.children}
    </button>
  );
}
