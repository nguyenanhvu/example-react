import "./App.css";
import Button from "./button";

function App() {
  return (
    <div className="App">
      <Button kind="primary" onClick={() => console.log("clicked!")}>
        Hello World!
      </Button>
    </div>
  );
}

export default App;
